import { UserEntity } from '@app/shared/entities/user.entity';
import { newUserDto } from '../dtos/new-user.dto';
import { ExistingUserDTO } from '../dtos/login.dto';

export interface AuthServiceInterface {
  getUser(): Promise<UserEntity[]>;

  findByEmail(email: string): Promise<UserEntity>;

  hashPassword(password: string): Promise<string>;

  register(newUser: newUserDto): Promise<UserEntity>;

  validateUser(email: string, password: string): Promise<UserEntity>;

  login(existingUser: Readonly<ExistingUserDTO>): Promise<{
    token: string;
    user: UserEntity;
  }>;

  verifyJwt(jwt: string): Promise<{ user: UserEntity; exp: number }>;
}
