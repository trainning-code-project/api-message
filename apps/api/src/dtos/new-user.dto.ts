export class newUserDto {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}
