import { RmqContext, RmqOptions } from '@nestjs/microservices';

export interface SharedServiceInterface {
  getRmqOption(queue: string): RmqOptions;
  acknowledgeMessage(context: RmqContext): void;
}
