import { UserEntity } from '@app/shared/entities/user.entity';
import { BaseInterfaceRepository } from '../repositories/base/base.interface.repository';

/*UserRepositoryInterface sẽ kế thừa tất cả các
phương thức được định nghĩa trong BaseInterfaceRepository và 
áp dụng cho UserEntity*/

export interface UserRepositoryInterface
  extends BaseInterfaceRepository<UserEntity> {}
